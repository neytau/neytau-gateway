import Joi from 'joi'
import Paths from '../conf/Paths'
import { consumeQueue } from '../conf/Rabbit'
import { receivedEvent, handleAlert } from '../controllers/SocketController'

module.exports = (server, io) => {
    io.on('connection', socket => {
        socket.on('tookPicture', picture => {
            //receivedEvent('tookPicture', picture)
        })

        socket.on('typed', typing => {
            //receivedEvent('typed', typing)
        })

        socket.on('mouved', mouvement => {
            receivedEvent('mouved', mouvement)
        })

        socket.on('clicked', click => {
            receivedEvent('clicked', click)
        })

        socket.on('askedHelp', post => {
            console.log('HEEEEEELP', post)
        })
    })

    consumeQueue('alert', (msg, ack) => {
        console.log('ALERT ==> ', msg)
        io.emit(msg, {})
        ack()
    })

    consumeQueue('askedHelp', (msg, ack) => {
        console.log('HEEEEEELP ==> ', msg)
        ack()
    })
}
