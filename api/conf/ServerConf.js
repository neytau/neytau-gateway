import vision from 'vision'
import inert from 'inert'
import swaggered from 'hapi-swagger'
import Boom from 'boom'

require('dotenv').config()

const PORT = process.env.PORT || 3333
const HOST = process.env.HOST || '127.0.0.1'

const hapiConf = {
    port: PORT,
    host: HOST,
    routes: { cors: true }
}

const socketAuthMiddleware = (socket, next) => {
    return next()
}

const swaggerConf = [
    vision,
    inert,
    {
        plugin: swaggered,
        options: {
            info: {
                title: 'NEYTAU API',
                description: 'Neytau\'s REST API documentation',
                version: '1.0'
            }
        }
    }
]

export { hapiConf, swaggerConf, socketAuthMiddleware }
