import amqp from 'amqplib'

require('dotenv').config()

const QUEUE_URL = `amqp://${process.env.QUEUE_URL || 'localhost:5672'}`

const queue = amqp.connect(QUEUE_URL)

const sendToQueue = (e, obj) => {
    queue.then(con => con.createChannel())
    .then(ch => ch.assertQueue(e).then(() => {
        return ch.sendToQueue(e, new Buffer(JSON.stringify(obj)))
    }))
    .catch(console.warn)
}

const consumeQueue = (q, cb) => {
    queue.then(con => con.createChannel())
    .then(ch => ch.assertQueue(q).then(ok => {
        return ch.consume(q, msg => {
            const ack = () => ch.ack(msg)
            cb(msg.content.toString(), ack)
        })
    })).catch(console.warn)
}


export { queue, sendToQueue, consumeQueue }
