import { sendToQueue } from '../conf/Rabbit'
import { client } from '../conf/RedisConf'

export const receivedEvent = (e, obj) => {
    client.HGET(obj.post, 'url', (err, rep) => {
        if (err) throw err
        if (rep && rep === obj.url) {
            sendToQueue(e, obj)
        } else {
            client.MULTI()
            .HSET(obj.post, 'url', obj.url)
            .HSET(obj.post, 'watchedAt', Date.now())
            .HSET(obj.post, 'viewedAt', Date.now())
            .HSET(obj.post, 'alerted', 0)
            .DEL(`${obj.post}:clicked`)
            .DEL(`${obj.post}:mouved`)
            .DEL(`${obj.post}:tookPicture`)
            .DEL(`${obj.post}:typed`)
            .EXEC((err, rep) => {
                if (err) throw err
                sendToQueue(e, obj)
            })
        }
    })
}
