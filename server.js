import Hapi from 'hapi'
import Nes from 'nes'
import { hapiConf, swaggerConf, socketAuthMiddleware } from './api/conf/ServerConf'
import Routes from './api/routes/Routes'
import socket from 'socket.io'

require('dotenv').config()

/* Check .env file and variables * /
if (!process.env.PG_CON) {
  throw 'Make sure you defined PG_CON in your .env file'
}
/* */

/* Start Web server */
const server = new Hapi.Server(hapiConf)

/*Register for Websockets*/
const io = socket.listen(server.listener)

const init = async () => {

    /*Register for Websockets*/
    //await server.register(Nes)

    /* Register for swagger documentation */
    await server.register(swaggerConf)

    /* Adding routes to the Server */
    Routes(server, io)

    /* Starting server */
    await server.start()
    console.log(`Server running at: ${server.info.uri}`)
}

init()
